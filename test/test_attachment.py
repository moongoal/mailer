#!/usr/bin/env python3

import os
import sys
import io
import unittest

sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))

import mailer


class AttachmentTestCase(unittest.TestCase):
  def test_data(self):
    dataraw = 'Some random test'
    data = io.StringIO(dataraw)
    fname = 'fname.txt'
    mimetype = 'text/plain'
    attach1 = mailer.Attachment(fname, data, mimetype)

    self.assertIsNone(attach1._Attachment__data)
    self.assertEqual(attach1.data, dataraw)
    self.assertIsNotNone(attach1._Attachment__data)

if __name__ == '__main__':
  unittest.main()
